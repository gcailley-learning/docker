DROP TABLE IF EXISTS assures;
DROP TABLE IF EXISTS dossiers_sante;


CREATE TABLE assures(
   id INT GENERATED ALWAYS AS IDENTITY,
   name VARCHAR(255) NOT NULL,
   phone VARCHAR(15),
   email VARCHAR(100),
   PRIMARY KEY(id)
);

CREATE TABLE dossiers_sante(
   id INT GENERATED ALWAYS AS IDENTITY,
   description VARCHAR(255) NOT NULL,
   ass_id INT,
   PRIMARY KEY(id),
      CONSTRAINT fk_dossier_assure
      FOREIGN KEY(ass_id) 
	  REFERENCES assures(id)
);

